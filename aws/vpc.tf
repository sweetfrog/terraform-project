
resource "aws_vpc" "Main" {
  cidr_block       = var.main_vpc_cidr
  instance_tenancy = "default"

  tags = {
    Name = "edwin-vpc"
  }
}

resource "aws_internet_gateway" "IGW"{
   vpc_id =  aws_vpc.Main.id
   tags = {
     Name = "edwin-igw"
   }
}

resource "aws_subnet" "publicsubnets1a" {
  vpc_id     = aws_vpc.Main.id
  cidr_block = var.public_subnets_1a
  tags = {
    Name = "edwin-subnet-public-1a"
  }
}

resource "aws_subnet" "publicsubnets1b" {
  vpc_id     = aws_vpc.Main.id
  cidr_block = var.public_subnets_1b
  tags = {
    Name = "edwin-subnet-public-1b"
  }
}

resource "aws_subnet" "privatesubnets1a" {
  vpc_id     = aws_vpc.Main.id
  cidr_block = var.private_subnets_1a
  tags = {
    Name = "edwin-subnet-private-1a"
  }
}

resource "aws_subnet" "privatesubnets1b" {
  vpc_id     = aws_vpc.Main.id
  cidr_block = var.private_subnets_1b
  tags = {
    Name = "edwin-subnet-private-1b"
  }
}

resource "aws_route_table" "PublicRT" {
  vpc_id = aws_vpc.Main.id

  route {
      cidr_block = "10.0.1.0/24"
      gateway_id = aws_internet_gateway.IGW.id
    }

  tags = {
    Name = "edwin-public-route"
  }
}

resource "aws_route_table" "PrivateRT" {
  vpc_id = aws_vpc.Main.id
  tags = {
    Name = "edwin-private-route"
  }
}

resource "aws_route_table_association" "PublicRTassociation1a" {
  subnet_id      = aws_subnet.publicsubnets1a.id
  route_table_id = aws_route_table.PublicRT.id
}

resource "aws_route_table_association" "PublicRTassociation1b" {
  subnet_id      = aws_subnet.publicsubnets1b.id
  route_table_id = aws_route_table.PublicRT.id
}

resource "aws_route_table_association" "PrivateRTassociation1a" {
  subnet_id      = aws_subnet.privatesubnets1a.id
  route_table_id = aws_route_table.PrivateRT.id
}

resource "aws_route_table_association" "PrivateRTassociation1b" {
  subnet_id      = aws_subnet.privatesubnets1b.id
  route_table_id = aws_route_table.PrivateRT.id
}
