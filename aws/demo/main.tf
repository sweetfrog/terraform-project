module "demo" {
  source = "../modules/vpc"
  vpc_name  = "edwin"
  region = "us-east-1"
  main_vpc_cidr = "10.0.0.0/16"
  public_subnets_1a = "10.0.1.0/24"
  public_subnets_1b = "10.0.2.0/24"
  private_subnets_1a = "10.0.3.0/24"
  private_subnets_1b = "10.0.4.0/24"
}
