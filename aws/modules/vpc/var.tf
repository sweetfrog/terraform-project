variable "vpc_name" {
  type = string
}
variable "region" {
  default ="us-east-1"
}
 variable "main_vpc_cidr" {
  default = "10.0.0.0/16"
 }
 variable "public_subnets_1a" {
  default = "10.0.1.0/24"
 }
  variable "public_subnets_1b" {
    default = "10.0.2.0/24"
  }
 variable "private_subnets_1a" {
  default = "10.0.3.0/24"
 }
 variable "private_subnets_1b" {
  default = "10.0.4.0/24"
 }
