/*
1. here you create an empty resource play
2. apply the terraform import aws_instance.import_ec2 <ec2 id> command
3. terraform import aws_instance.import_ec2 i-07e61ccb859a9c2ef
4. once imported execute the "terraform show" command and copy the imported resource into your main.tf
5. execute "terraform plan" to reconcile the config between the main.tf and the state file
6. note, not all the object are configurable. so you have to delete some of the stuff you imported from the states file
7. delete all the default attribute and keep the important once ( eg ami, instance_type, key_name, security_groups)
8. now after terraform can now manage the resource. so if you execute "terraform destroy". it should be able to delete the resource
*/
resource "aws_instance" "import_ec2" {
    ami                                  = "ami-02e136e904f3da870"
    instance_type                        = "t2.micro"
    key_name                             = "web"
    user_data                            = "5516421764c1d4439d7fdaf48468965a0dc6a1ff"
    security_groups                      = [
        "import security group",
    ]
    tags                                 = {
        "Name" = "import-server"
    }
    tags_all                             = {
        "Name" = "import-server"
    }
}
